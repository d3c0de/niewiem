
// title screen colours
*= $0800
	.fill 1000, $01

*= $1000
.import binary "../assets/music/bozenka_game_$1000.bin"
// music uses zp $fd-fe

charGfxColor:
#import "../assets/levels/1/1.colour"

*= $2000
.import binary "../assets/arts/cycki.bmp.bin"

//char map (screen)
*= $4000
#import "../assets/levels/1/1.char"

//char set 
*= $4800
#import "../assets/fonts/undersea.asm"

// sprites
*= $5000 
.const sprL1 = (* - videoBank) / 64
.import binary "../assets/sprites/l1.raw"
.byte 0
.const sprL2 = (* - videoBank) / 64
.import binary "../assets/sprites/l2.raw"
.byte 0
.const sprR1 = (* - videoBank) / 64
.import binary "../assets/sprites/r1.raw"
.byte 0
.const sprR2 = (* - videoBank) / 64
.import binary "../assets/sprites/r2.raw"
.byte 0

/////////////////////////////////////////

.macro setSpritePtr( _sprId, _sprPtr ) {
		lda #_sprPtr
		sta SPRITE_SELECTION_OFFSET + _sprId + videoScreenMem
}


/////////////////////////////////////////

gameOverText:
	.text "game over                               "
	.byte 0

*= $8000

.namespace loop {
    start:
		sei
		cld
		jmp !+

	.text "greetings to elite crackers who *first-release* "
	.text "just about anything on csdb to score stats"
	
!:		lda #0
		jsr $1000
		
		jsr initGame
		lda #0 
		sta SPRITES_VISIBLE		
		
		InitInterrupts()
		Interrupt( mainIRQ, $ff);
		cli
		
		jsr titleScreen
		
		SetBackground(COLOR_WHITE, COLOR_BLUE)
		
		SetVideo(videoBank, videoScreenMem)
		SetFontAndLayout($800, 0)
		
		SetVideoMode( CHARACTER | SINGLECOLOR )
		
		ldy #0
copyColor:
		lda charGfxColor,y
		sta COLORS_BASE,y
		lda charGfxColor + $100,y
		sta COLORS_BASE + $100,y
		lda charGfxColor + $200,y
		sta COLORS_BASE + $200,y
		lda charGfxColor + $300,y
		sta COLORS_BASE + $300,y
		dey
		bne copyColor
		
		lda #$ff
		sta SPRITES_VISIBLE

		lda #$ff
		sta SPRITES_MULTICOLOR_ENABLED
		
		lda #COLOR_YELLOW
		sta SPRITES_MULTICOLOR_1

		lda #COLOR_RED
		sta SPRITES_MULTICOLOR_2

		lda #COLOR_WHITE
		sta SPRITE0_COLOR

	mainGameLoop:

		jmp mainGameLoop

// ------------------------------------
	mainIRQ:
		AcknowledgeRasterInterrupt()
		StartInterrupt()
		
		jsr doGameOver
checkGameOverInIRQ:
		lda gameOverFlag 
		bne letTheMusicPlay // if game over player control stops
		
		jsr doPlayer

!:

letTheMusicPlay:
		jsr $1003
		
		ReturnFromInterrupt()
	}
// ----------------------------------------

	doGameOver:
.namespace GameOver {
		lda gameOverFlag
		and #1
		beq noGameOver
		inc $d021
		
		ldx #0
gameOverPrint:
		lda gameOverText,x
		beq !+
		sta videoScreenMem,x
		inx
		jmp gameOverPrint
!:
		jsr readJoy2
		lda joyY
		beq gameOverRts
		jmp loop.start

noGameOver:
gameOverRts:
		
		rts
}
	
	doPlayer:

.namespace joySupport {	
		jsr readJoy2
		bcs !+ // fire btn moves player up, C==0 is fire pressed
		
		lda btnHold
		bne leftRight // if fire is hold pressed then dont go up, need to cycle 
		
		inc btnHold
		
		// go up w/limit
		lda posY
		cmp #SPRITE_POSY_TOP + 16 + 2
		bcc leftRight // top Reached	
		
		dec posY 
		dec posY
		//dec posY
		
		jmp leftRight 
!:
		lda #0
		sta btnHold

leftRight:		
		lda joyX
		and #1
		beq !+
		setSpritePtr(0, sprR1)
!:		
		lda joyX
		and #128
		beq !+
		setSpritePtr(0, sprL1)
!:
		lda posX
		clc
		adc joyX
	// 9th spr x bit support
/*
		bcc noXboundary
		inc SPRITES_POSX_MSB // toggle SPRITE0_POSX8
		lda SPRITES_POSX_MSB
		lsr
		bcc lt256
gt255:
		lda #(SPRITE_POSX_RIGHTMOST & $ff) 
		sta posX
		
lt256:	
		lda #SPRITE_POSX_LEFTMOST
		sta posX

noXboundary:		
*/		
		sta posX
		
		
		jsr fade
		
		lda posX
		sta SPRITE0_POSX
		lda posY
		sta SPRITE0_POSY

		lda SPRITE_CHAR_COLLISION
		and #1
		beq !+
		
		lda #1
		sta gameOverFlag
!:		
		rts

	fade:
		lda posY
		cmp #SPRITE_POSY_BOTTOM + 10
		bne !+
		rts
	!:
		lda fadeCounter
		bne !+
		
		lda #fadeDelay
		sta fadeCounter
		
		inc posY
	!:
		dec fadeCounter	
		
		rts
		
btnHold:
		.byte 0	// 0 == not hold


fadeCounter:
		.byte 0
}

		initGame:
		lda #128
		sta posY
		sta posX
		lda #0
		sta gameOverFlag
		
		setSpritePtr(0, sprL1)
		
		// TODO
		// lives 
		// points
		rts

gameOverFlag:
.byte 0		
		
velX:
.byte 0
velY:
.byte 0

posX:
.byte 0
posXhi: // TODO 
.byte 0
posY:
.byte 0	
	

	