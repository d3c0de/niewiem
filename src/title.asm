titleScreen:

.namespace title {
		SetBackground(COLOR_BLACK, COLOR_BLACK)
		SetVideo(0, $2000)
		SetFontAndLayout($2000, $800) // $2000 for bitmap
		
		SetVideoMode( BITMAP | SINGLECOLOR )
		
	mainLoop:

		jsr readJoy2
		bcs mainLoop
		
		rts
		
		jmp mainLoop
	}