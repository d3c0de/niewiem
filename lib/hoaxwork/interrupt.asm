#importonce

.import source "constants.asm"
.import source "registers.asm"
//.import source "cyclecount.asm"
.import source "zeropage.asm"
.import source "debug.asm"

.const INTERRUPT_STORE = $1b
.const INTERRUPT_STORE_A = INTERRUPT_STORE+0 
.const INTERRUPT_STORE_X = INTERRUPT_STORE+1 
.const INTERRUPT_STORE_Y = INTERRUPT_STORE+2 
.const INTERRUPT_STORE_1 = INTERRUPT_STORE+3

:ReserveRangeZP( INTERRUPT_STORE_A, INTERRUPT_STORE_1+1, "Interrupt Register Store" )

// Creates a standard interrupt on any given rasterline. Note that the
// a sei must be done before and a cli after.
// \param _irq_routine handle or address to where the interrupt routine is
// \param _rasterline rasterline where interrupt shall be triggered
.macro Interrupt( _irq_routine, _rasterline )
{
	lda #$35
	sta $01   // Diable most ROM, IO-area (D000-DFFF) is still available
	:InitInterrupts()
	:SetIRQ( _irq_routine )
	:SetIRQLine( _rasterline )
	:DisableNMI()
	:AcknowledgeRasterInterrupt()
}

// This macro will disable the CIA timer interrupts, enable IRQ interrupt
.macro InitInterrupts()
{
	// clear timer interrupts
	lda #$7f
	sta CIA1_INTERRUPT_CONTROL_REGISTER
	sta CIA2_INTERRUPT_CONTROL_REGISTER
	// Acknowledge timer interrupts
	lda CIA1_INTERRUPT_CONTROL_REGISTER
	lda CIA2_INTERRUPT_CONTROL_REGISTER

	// set interrupt mask
	lda #IRM_RASTER_INTERRUPT
	sta INTERRUPT_REQUEST_MASK
}

.macro AcknowledgeRasterInterrupt()
{
	asl INTERRUPT_STATUS_MASK // $d019
	//lda INTERRUPT_STATUS_MASK
	//and #$fe
	//sta INTERRUPT_STATUS_MASK
}

.macro AckAllVicIRQs()
{
	pha
	lda #0
	sta INTERRUPT_STATUS_MASK // $d019
	pla
}

.macro NextInterrupt( _irq_routine, _rasterline )
{
	:SetIRQ( _irq_routine )
	:SetIRQLine( _rasterline )
}

// Sets the IRQ location
.macro SetIRQ( _jump_location )
{
	lda #<_jump_location
	sta IRQ_JMP_LO
	lda #>_jump_location
	sta IRQ_JMP_HI
}

// Sets the IRQ line
.macro SetIRQLine( _rasterline )
{
	.if ( _rasterline >= $100 )
	{
		.error "SetIRQLine does not support rasterlines above $ff"
	}
	lda #<_rasterline
	sta RASTERLINE_REGISTER
}

// Disables STOP/RESTORE capabilities (must reset to end program).
.macro DisableNMI()
{
	lda #RTI
	sta $ff
	lda #$ff
	ldx #0
	sta NMI_JMP_LO
	stx NMI_JMP_HI
	sta CRS_JMP_LO
	stx CRS_JMP_HI
}

// This macro will stable the raster for 1 cycle at the end of the rasterline
// \param _timingvalue the number of cycles to wait for the end of the line
// \note add 100 to the timingvalue and adjust until it flickers between two
// colors
/*
.macro StableCycle( _timingvalue )
{
	:WaitCycles( mod(_timingvalue, 100 ) )
	lda $d012
	.if ( _timingvalue > 100 )
	{
		lda $d012
		sta $d020
	}
	cmp $d012
	beq *+2
}*/

// Same as above, except the stabling counts for two cycles
/*
.macro StableCycle2( _timingvalue )
{
	:WaitCycles( mod(_timingvalue, 100 ) )
	lda $d012
	.if ( _timingvalue > 100 )
	{
		lda $d012
		sta $d020
	}
	cmp $d012
	bne *+4
	bit $ea // spend 3 cycles
	
}*/

// Faster way of stabling raster, uses only 2 lines per interrupt
// Note that the IRQ must be reset after the use if this function
// The value of "_tuningparam" should be timed, since it depends on
// whether there are any sprites on the line, which sprites are used
// and if there is a badline there. To check the tuning parameter, add 100 to the value
// The IRQ is complete with all storing of registers and processor port address
// Return from the interrupt using rts.
/*
.macro StableIRQ( _routine, _rasterline, _tuningparam )
{
	:NextInterrupt( internalIrq, _rasterline )
	jmp continue
internalIrq:
	sta restoreA+1
	stx restoreX+1
	sty restoreY+1
	lda $01
	sta restore1+1
	:SetIRQ( secondaryIrq )
	:SetIRQLine( _rasterline+1 )
	tsx
	:AcknowledgeInterrupt()
	cli
	:WaitNops( 64 )
	jmp *
	.byte $02 // Make sure 10 cycles of nops are enough
secondaryIrq:
	txs
	:WaitCycles( mod( _tuningparam, 100 ) ) // SThis rastertime can be used some other way
	lda #_rasterline+1
.if ( _tuningparam > 100 )
{
	lda $d012
	sta $d020
}
else
{
	cmp $d012
	beq *+2
}
	jsr _routine
restore1:
	lda #0
	sta $01
restoreA:
	lda #0
restoreX:
	ldx #0
restoreY:
	ldy #0
	asl INTERRUPT_STATUS_MASK
	rti
continue:
}
*/

/*
// This routine will stabilize a raster (using 3 rasterlines in the process)
// When figuring out where the stabilizing will occur, 
// use _showtiming value 1-3 to display them.
// always tune _delay1 first (using _showtiming = 1).
// When finished, use _showtiming = 0.
// \param _delay1 cycles until first raster flicker one first line
// \param _delay2 cycles until first raster flicker two second line
// \param _delay3 cycles until first raster flicker three third line
// \param _showtiming set to 1-3 to show flicker location
// When showing flicker location, the screen should flash between two colors
// Typically _delay1, _delay2 and _delay3 would be around (15, 53, 53)
.macro StableRaster( _delay1, _delay2, _delay3, _showtiming )
{
	:WaitCycles( _delay1 )
	lda $d012
	.if ( _showtiming == 1 )
	{
		lda $d012
		sta $d021
	}
	else
	{
		cmp $d012
		beq *+2

		:WaitCycles( _delay2 )
		lda $d012
		.if ( _showtiming == 2 )
		{
			lda $d012
			sta $d021
		}
		else
		{
			cmp $d012
			beq *+2

			:WaitCycles( _delay3 )
			lda $d012
			.if ( _showtiming == 3 )
			{
				lda $d012
				sta $d021
			}
			else
			{
				cmp $d012
				beq *+2
			}
		}
	}
}
*/

// This function is useful for waiting for a rasterline
// \param _line_no rasterline to wait for
.macro WaitRaster( _line_no )
{
	lda #_line_no
	cmp $d012
	bne *-3
}

// If you need to do computations outside the raster interrupt, start your 
// interrupt with this macro, and end with :ReturnFromInterrupt().
// It will store away A, X, Y before they are changed in the routine.
.macro StartInterrupt()
{
	sta INTERRUPT_STORE_A
	stx INTERRUPT_STORE_X
	sty INTERRUPT_STORE_Y
	lda PROCESSOR_PORT
	sta INTERRUPT_STORE_1
//	pha
//	txa
//	pha
//	tya
//	pha
//	lda PROCESSOR_PORT
//	pha
	lda #PP_VIC_ONLY 
	sta PROCESSOR_PORT
	.if ( DebugOn( SHOW_INTERRUPTTIME ) )
	{
		inc $d020
	}
}

// See StartInterrupt()
.macro ReturnFromInterrupt()
{
	.if ( DebugOn( SHOW_INTERRUPTTIME ) )
	{
		dec $d020
	}
	asl $d019
/*
	pla
	sta PROCESSOR_PORT
	pla
	tay
	pla
	tax
	pla
*/
	lda INTERRUPT_STORE_1
	sta PROCESSOR_PORT
	lda INTERRUPT_STORE_A
	ldx INTERRUPT_STORE_X
	ldy INTERRUPT_STORE_Y
	rti
}

.macro ResetDefaults()
{
	sei
	lda #$2f
	sta $00
	lda #$37
	sta $01
	lda #0
	sta $d418 // Volume to 0
	sta $d01a // Clear all interrupt flags, except NMI
	sta $d011 // Clear screen also
	cli
}
