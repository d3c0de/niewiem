#importonce
.import source "debug.asm"
// File containing zeropage helper functions.
// The zero-pages can be aquired dynamically through the AllocateZP macro
// Before any zero-pages are aquired, use the ReserveZP macro to avoid
// reusing zero-pages that other external functions have in use (i.e. music/loader)

.var GlobalZP = Hashtable()
.const MIN_ZEROPAGE = $00
.const MAX_ZEROPAGE = $ff

.const ZP_TEMP_BASE = $c0

.const ZP_TEMP_0_LOCAL = ZP_TEMP_BASE+0
.const ZP_TEMP_1_LOCAL = ZP_TEMP_BASE+1
.const ZP_TEMP_2_LOCAL = ZP_TEMP_BASE+2
.const ZP_TEMP_3_LOCAL = ZP_TEMP_BASE+3
.const ZP_TEMP_4_LOCAL = ZP_TEMP_BASE+4
.const ZP_TEMP_5_LOCAL = ZP_TEMP_BASE+5
.const ZP_TEMP_6_LOCAL = ZP_TEMP_BASE+6
.const ZP_TEMP_7_LOCAL = ZP_TEMP_BASE+7

.const ZP_TEMP_0_GLOBAL = ZP_TEMP_BASE+8
.const ZP_TEMP_1_GLOBAL = ZP_TEMP_BASE+9
.const ZP_TEMP_2_GLOBAL = ZP_TEMP_BASE+10
.const ZP_TEMP_3_GLOBAL = ZP_TEMP_BASE+11
.const ZP_TEMP_4_GLOBAL = ZP_TEMP_BASE+12
.const ZP_TEMP_5_GLOBAL = ZP_TEMP_BASE+13
.const ZP_TEMP_6_GLOBAL = ZP_TEMP_BASE+14
.const ZP_TEMP_7_GLOBAL = ZP_TEMP_BASE+15

.const ZP_TEMPW_0_LOCAL = ZP_TEMP_BASE+16
.const ZP_TEMPW_1_LOCAL = ZP_TEMP_BASE+18
.const ZP_TEMPW_2_LOCAL = ZP_TEMP_BASE+20
.const ZP_TEMPW_3_LOCAL = ZP_TEMP_BASE+22
.const ZP_TEMPW_4_LOCAL = ZP_TEMP_BASE+24
.const ZP_TEMPW_5_LOCAL = ZP_TEMP_BASE+26

// These zeropages are reserved for the macros.
.const ZP_TEMP_MACRO0 = ZP_TEMP_BASE+28
.const ZP_TEMP_MACRO1 = ZP_TEMP_BASE+29
.const ZP_TEMP_MACRO2 = ZP_TEMP_BASE+30
.const ZP_TEMP_MACRO3 = ZP_TEMP_BASE+31

.for ( var i = MIN_ZEROPAGE; i <= MAX_ZEROPAGE; i++ )
{
	.eval GlobalZP.put( i, "" )
}

.macro PrintZPList()
{
	.for ( var i = MIN_ZEROPAGE; i <= MAX_ZEROPAGE; i++ )
	{
		.if( GlobalZP.get( i ) != "" )
		{
			.print "$" + toHexString( i ) + "=" + GlobalZP.get( i )
		}
	}
}

.macro LoadImm( _zeropage, _memory )
{
	//.assert "ZeroPage > 255, ZP should be first parameter", _zeropage<256, true
	lda #<_memory
	sta _zeropage
	lda #>_memory
	sta _zeropage+1
}

// This macro will add a zp to the reserve-list, making it unavaliable for picking zp'es
// This macro should be called before any zero pages are allocated to avoid conflict.
.macro ReserveZP( _zp, _name )
{
	.if ( GlobalZP.get( _zp ) != "" )
	{
	.error "ZP " + _zp + " conflicts for " + _name + " and " + GlobalZP.get( _zp )
	}
	.eval GlobalZP.put( _zp, _name )
}

.macro ReserveRangeZP( _zpfirst, _zplast, _name )
{
	.for ( var i = _zpfirst; i <= _zplast; i++ )
	{
		:ReserveZP( i, _name + "_" + toIntString( i - _zpfirst ) )
	}
}

.macro ReleaseZP( _zp )
{
	.eval GlobalZP.put( _zp, true )
}

// Allocate a byte zeropage 
.function AllocateZP( _name )
{
	.for ( var zp = MIN_ZEROPAGE; zp < MAX_ZEROPAGE; zp++ )
	{
		.if ( GlobalZP.get( zp ) == "" )
		{
			.eval GlobalZP.put( zp, _name )
			.if ( DebugOn( TRACE_ZEROPAGE ) )
			{
				.print "Allocated ZP $" + toHexString(zp) + " to " + _name
			}
			.return zp
		}
	}
	.error "No more zero pages left"
}

// Allocate a word zeropage
.function AllocateWordZP( _name )
{
	.for ( var zp = MIN_ZEROPAGE; zp < MAX_ZEROPAGE; zp++ )
	{
		.if ( GlobalZP.get( zp ) == "" )
		{
			.if ( GlobalZP.get( zp+1 ) == "" )
			{
				.eval GlobalZP.put( zp, _name+"_LO" )
				.eval GlobalZP.put( zp+1, _name+"_HI" )
				.if ( DebugOn( TRACE_ZEROPAGE ) )
				{
					.print "Allocated ZP $" + toHexString(zp) + "/$" + toHexString(zp+1) + " to " + _name
				}
				.return zp
			}
			else
			{
				.if ( DebugOn( TRACE_ZEROPAGE ) )
				{
					.print "Valid ZP found at $" + toHexString( zp ) + ", but next one was taken"
				}
			}
		}
	}
	.error "No more zero pages left, or no places for two consecutive zero pages"
}

:ReserveZP( 0, "CPU" )
:ReserveZP( 1, "CPU" )
:ReserveRangeZP( ZP_TEMP_BASE, ZP_TEMP_BASE+31, "GLOBALS" )
