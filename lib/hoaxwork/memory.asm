// Memory management macros
// The following macros exist in this file:
// FillMem( memory_start, memory_end, value )
// MoveMem( memory_start, memory_end, destination )

// Fills memory from start to end with a value
// \param _memory_start start of memory to fill
// \param _memory_end last memory to fill
// \param _value value to fill
// \registers a,x,y (x used if range>=$800)
// \example :FillMem( $1000, 0, $100 )
.macro FillMem( _destination, _value, _length )
{
  .if ( _length == 0 )
  {
    .print "length of fill is 0, possible parameter confusion"
  } else {
    lda #_value
    .if ( _length < $100 )
    {
      ldy #_length
!loop:
      sta _destination-1,y
      dey
      bne !loop-
    }
    else 
    {
      .if ( _length <= $800 )
      {
        ldy #0
!loop:
        .for ( var i = 0; i < floor(_length/$100); i++ )
        {
          sta _destination+$100*i,y
        }
        .if ( mod( _length, $100 ) != 0 )
        {
          sta _destination+_length-$100,y
        }
	iny
	bne !loop-
      }
      else
      {
        .var lastblock = mod( _length, $100 )
        .if ( lastblock != 0 )
        {
          ldx #<_destination+_length-lastblock
          ldy #>_destination+_length-lastblock
	} else {
				ldx #<_destination+_length-$100
				ldy #>_destination+_length-$100
			}
			stx ZP_TEMP_MACRO0
			sty ZP_TEMP_MACRO1
			.if ( lastblock == 0 )
			{
				ldx #_length/$100
			} else {
				ldx #1+_length/$100
			}			
			ldy #lastblock
		!loop:
			dey
			sta (ZP_TEMP_MACRO0),y
			bne !loop-
			dec ZP_TEMP_MACRO1
			dex 
			bne !loop-
		}
	}
}
}

// \example :CopyMem( $1000, $2000, $100 )
.macro CopyMem( _source, _destination, _amount )
{
	.if ( _amount < $100 )
	{
		ldy #_amount
	!loop:
		lda _source-1,y
		sta _destination-1,y
		dey
		bne !loop-
	} else {
		.if ( _amount <= $800 )
		{
			ldy #0
		!loop:
			.for ( var i = 0; i < floor(_amount/$100); i++ )
			{
				lda _source+$100*i,y
				sta _destination+$100*i,y
			}
			.if ( mod( _amount, $100 ) != 0 )
			{
				lda _source+_amount-$100,y
				sta _destination+_amount-$100,y
			}
			iny
			bne !loop-
		} else {
			.var lastblock = mod( _amount, $100 )
			.if ( lastblock != 0 )
			{
				lda #<_source+_amount-lastblock
				ldy #>_source+_amount-lastblock
				sta ZP_TEMP_MACRO0
				sty ZP_TEMP_MACRO1
				lda #<_destination+_amount-lastblock
				ldy #>_destination+_amount-lastblock
				sta ZP_TEMP_MACRO2
				sty ZP_TEMP_MACRO3
			} else {
				lda #<_source+_amount-$100
				ldy #>_source+_amount-$100
				sta ZP_TEMP_MACRO0
				sty ZP_TEMP_MACRO1
				lda #<_destination+_amount-$100
				ldy #>_destination+_amount-$100
				sta ZP_TEMP_MACRO2
				sty ZP_TEMP_MACRO3
			}
			lda #0
			.if ( lastblock == 0 )
			{
				ldx #_amount/$100
			} else {
				ldx #1+_amount/$100
			}			
			ldy #lastblock
		!loop:
			dey
			lda (ZP_TEMP_MACRO0),y
			sta (ZP_TEMP_MACRO2),y
			cpy #0
			bne !loop-
			dec ZP_TEMP_MACRO1
			dec ZP_TEMP_MACRO3
			dex 
			bne !loop-
		}
	}
}

.macro RoundRobin( _memory, _amount )
{
	ldx _memory
	.for ( var index = 0; index < _amount-1; index++ )
	{
		lda _memory+index+1
		sta _memory+index
	}
	stx _memory+_amount-1
}

.macro ScrollMem( _memory, _amount )
{
	.for ( var index = 0; index < _amount-1; index++ )
	{
		lda _memory+index+1
		sta _memory+index
	}
}
