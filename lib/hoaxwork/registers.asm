#importonce

.const PROCESSOR_PORT_ACCESS            = $0000
.const PROCESSOR_PORT                   = $0001

.const VIC_BASE    = $d000
.const COLORS_BASE = $d800
.const CIA1_BASE   = $dc00
.const CIA2_BASE   = $dd00

// VIC Base Registers
.const SPRITES_POSX_BASE                = VIC_BASE+$00
.const SPRITES_POSY_BASE                = VIC_BASE+$01
.const SPRITES_COLOR_BASE               = VIC_BASE+$27

// All VIC registeres
.const SPRITE0_POSX                     = VIC_BASE+$00
.const SPRITE0_POSY                     = VIC_BASE+$01
.const SPRITE1_POSX                     = VIC_BASE+$02
.const SPRITE1_POSY                     = VIC_BASE+$03
.const SPRITE2_POSX                     = VIC_BASE+$04
.const SPRITE2_POSY                     = VIC_BASE+$05
.const SPRITE3_POSX                     = VIC_BASE+$06
.const SPRITE3_POSY                     = VIC_BASE+$07
.const SPRITE4_POSX                     = VIC_BASE+$08
.const SPRITE4_POSY                     = VIC_BASE+$09
.const SPRITE5_POSX                     = VIC_BASE+$0a
.const SPRITE5_POSY                     = VIC_BASE+$0b
.const SPRITE6_POSX                     = VIC_BASE+$0c
.const SPRITE6_POSY                     = VIC_BASE+$0d
.const SPRITE7_POSX                     = VIC_BASE+$0e
.const SPRITE7_POSY                     = VIC_BASE+$0f
.const SPRITES_POSX_MSB                 = VIC_BASE+$10
.const SCREEN_CONTROL_REGISTER_Y        = VIC_BASE+$11
.const RASTERLINE_REGISTER              = VIC_BASE+$12
.const LIGHTPEN_X                       = VIC_BASE+$13
.const LIGHTPEN_Y                       = VIC_BASE+$14
.const SPRITES_VISIBLE                  = VIC_BASE+$15
.const SCREEN_CONTROL_REGISTER_X        = VIC_BASE+$16
.const SPRITES_STRETCHY_ENABLED         = VIC_BASE+$17
.const SCREEN_CHARACTER_MEMORY_ADDRESS  = VIC_BASE+$18
.const INTERRUPT_STATUS_MASK            = VIC_BASE+$19
.const INTERRUPT_REQUEST_MASK           = VIC_BASE+$1a
.const SPRITES_PRIORITY_REGISTER        = VIC_BASE+$1b
.const SPRITES_MULTICOLOR_ENABLED       = VIC_BASE+$1c
.const SPRITES_STRETCHX_ENABLED         = VIC_BASE+$1d
.const SPRITE_SPRITE_COLLISION          = VIC_BASE+$1e
.const SPRITE_CHAR_COLLISION            = VIC_BASE+$1f
.const BORDER_COLOR_REGISTER            = VIC_BASE+$20
.const BACKGROUND_COLOR_REGISTER        = VIC_BASE+$21
.const CHARACTER_MULTICOLOR_1           = VIC_BASE+$22
.const CHARACTER_MULTICOLOR_2           = VIC_BASE+$23
.const CHARACTER_MULTICOLOR_3           = VIC_BASE+$24
.const SPRITES_MULTICOLOR_1             = VIC_BASE+$25
.const SPRITES_MULTICOLOR_2             = VIC_BASE+$26
.const SPRITE0_COLOR                    = VIC_BASE+$27
.const SPRITE1_COLOR                    = VIC_BASE+$28
.const SPRITE2_COLOR                    = VIC_BASE+$29
.const SPRITE3_COLOR                    = VIC_BASE+$2a
.const SPRITE4_COLOR                    = VIC_BASE+$2b
.const SPRITE5_COLOR                    = VIC_BASE+$2c
.const SPRITE6_COLOR                    = VIC_BASE+$2d
.const SPRITE7_COLOR                    = VIC_BASE+$2e

// CIA#1 Registers
.const PORTA_KEYBOARD_MATRIX_COLUMN     = CIA1_BASE+$00
.const PORTA_JOYSTICK2_REGISTER         = CIA1_BASE+$00
.const PORTB_KEYBOARD_MATRIX_ROW        = CIA1_BASE+$01
.const PORTB_JOYSTICK1_REGISTER         = CIA1_BASE+$01
.const PORTA_DATA_DIRECTION	        = CIA1_BASE+$02
.const PORTB_DATA_DIRECTION	        = CIA1_BASE+$03
.const CIA1_TIMERA_LO_REGISTER          = CIA1_BASE+$04
.const CIA1_TIMERA_HI_REGISTER          = CIA1_BASE+$05
.const CIA1_TIMERB_LO_REGISTER          = CIA1_BASE+$06
.const CIA1_TIMERB_HI_REGISTER          = CIA1_BASE+$07
.const TIME_OF_DAY_DECSEC               = CIA1_BASE+$08
.const TIME_OF_DAY_SECONDS              = CIA1_BASE+$09
.const TIME_OF_DAY_MINUTES              = CIA1_BASE+$0a
.const TIME_OF_DAY_HOURS                = CIA1_BASE+$0b
.const SERIAL_SHIFT_REGISTER            = CIA1_BASE+$0c
.const CIA1_INTERRUPT_CONTROL_REGISTER  = CIA1_BASE+$0d

.const PORTA_SERIAL_BUS_ACCESS		= CIA2_BASE+$00
.const PORTA_SERIAL_BUS_DIRECTION	= CIA2_BASE+$02
.const CIA2_INTERRUPT_CONTROL_REGISTER  = CIA2_BASE+$0d

.const BASIC_INTERRUPT_HANDLER          = $ea31
.const DEFAULT_INTERRUPT_HANDLER        = $ea81

.const BRK_JMP_LO                       = $fffa
.const BRK_JMP_HI                       = $fffb
.const NMI_JMP_LO                       = $fffa
.const NMI_JMP_HI                       = $fffb
.const CRS_JMP_LO                       = $fffc
.const CRS_JMP_HI                       = $fffd
.const IRQ_JMP_LO                       = $fffe
.const IRQ_JMP_HI                       = $ffff

// Aliases
.const VIC_BANK_SELECT_REGISTER = PORTA_SERIAL_BUS_ACCESS

