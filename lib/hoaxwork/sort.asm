#importonce
// This file contains macros for sorting algorithms
// The algorithms are not optimized for size, only speed


// This macro will perform an insertsort of one value (register A)
// \param _value points to where the value to be inserted is stored
// \param _buffer points to where the current dataset is located
// \param _count points to where in memory the count of the number
//               of values currently stored, this will be incremented
// \reg A, X, Y
.macro InsertSort( _value, _buffer, _count )
{
	ldx #0
	lda _buffer,x
loop1:
	cmp _value
	bcs insertFromHere
	inx
	cpx _count
	bne loop1
	lda _value
	sta _buffer,x
	jmp finished
insertFromHere:
loop2:
	ldy _buffer,x
	sta _buffer,x
	tya
	inx
	cpx _count
	bne loop2
finished:
	inc _count
}

// This macro will perform an insert-sort of one value, but the stored data
// will be the index of the value
// \param _value points to where the value to be sorted is located
// \param _indexvalue points to where the index of _value is found
// \param _indexbuffer points to the sorted buffer of indexes
// \param _comparebuffer points to the buffer of values to compare with
// \param _count points to where the valuecount is currently stored
//               this value will be incremented
// \reg A, X, Y
.macro InsertSortIndex( _value, _indexvalue, _indexbuffer, _comparebuffer, _count )
{
	ldy #0
	cpy _count
	bne loopScan
	lda _indexvalue
	sta _indexbuffer
	jmp finished
loopScan:
	ldx _indexbuffer,y
	lda _comparebuffer,x
	cmp _value
	bcs insertFromHere
	iny
	cpy _count
	bne loopScan
	// insert last
	lda _indexvalue
	sta _indexbuffer,y
	jmp finished
insertFromHere:
	lda _indexvalue
loopInsert:
	ldx _indexbuffer,y
	sta _indexbuffer,y
	txa
	iny
	cpy _count
	bne loopInsert
	sta _indexbuffer,y
finished:
	ldx _count
	lda _value
	sta _comparebuffer,x
	inc _count
}

// This macro will perform an insert-sort of one value, but the stored data
// will be the index of the value
// \param A value to be sorted
// \param _indexvalue index of the value in A
// \param _indexbuffer points to the sorted buffer of indexes
// \param _comparebuffer points to the buffer of values to compare with
// \param _count points to where the valuecount is currently stored
//               this value will be incremented
// \reg A, X, Y
.macro InsertSortIndexFast( _indexvalue, _indexbuffer, _comparebuffer, _count )
{
	ldy #0
!loop:
	ldx _indexbuffer,y
	cmp _comparebuffer,x
	bcc insertFromHere
	iny
	cpy #_count
	bne !loop-
	// insert last
	lda #_indexvalue
	sta _indexbuffer,x
	jmp finished
insertFromHere:
	lda #_indexvalue
!loop:
	ldx _indexbuffer,y
	sta _indexbuffer,y
	txa
	iny
	cpy #_count
	bne !loop-
finished:
}

// This macro will perform a bubble sort of an array
// Bubble sort is the slowest but most compact way
.macro BubbleSort( _valuebuffer, _length ) 
{	
untilFinished:
	lda #0
	sta swapCount+1
	tay
loopY:
	lda _valuebuffer,y
	cmp _valuebuffer+1,y
	bcc noSwap
	// Swap
	pha
	lda _valuebuffer+1,y
	sta _valuebuffer,y
	pla
	sta _valuebuffer+1,y
	inc swapCount+1
noSwap:
	iny
	cpy #_length-1
	bne loopY
swapCount:
	lda #0
	bne untilFinished

}
// This macro will perform a bubble sort of indexes
.macro BubbleSortIndex( _valuebuffer, _indexbuffer, _length )
{
untilFinished:
	lda #0
	sta swapCount+1
	tay
loopY:
	ldx _indexbuffer+0,y
	lda _valuebuffer,x
	ldx _indexbuffer+1,y
	cmp _valuebuffer,x
	bcs noSwap
	// Swap elements
	lda _indexbuffer,y
	pha
	lda _indexbuffer+1,y
	sta _indexbuffer,y
	pla
	sta _indexbuffer+1,y
	inc swapCount+1
noSwap:
	iny
	cpy #_length-1
	bne loopY
swapCount:
	lda #0
	bne untilFinished
}

