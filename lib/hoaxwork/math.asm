#importonce

.const PI = 3.1416

.macro Asl( _repetition )
{
	.for ( var i = 0; i < _repetition; i++ ) asl
}

.macro Lsr( _repetition )
{
	.for ( var i = 0; i < _repetition; i++ ) lsr
}

.macro IncWord( _memory )
{
	inc _memory
	bne !skip+
	inc _memory+1
!skip:
}

.macro CmpWordConst( _memory, _value )
{
	lda _memory+1
	cmp #>_value
	bne !skip+
	lda _memory
	cmp #<_value
!skip:
}

// _memory1 > _memory2 ? (as signed word values)
.macro IsHigherMem( _memory1, _memory2, _branchlabel )
{
	lda _memory1+1
	cmp _memory2+1
	bpl _branchlabel
	bne !fin+
	lda _memory1
	cmp _memory2
	bcc _branchlabel
!fin:
}

// _memory1 < _memory2 ? (as signed word values)
.macro IsLowerMem( _memory1, _memory2, _branchlabel )
{
	lda _memory1+1
	cmp _memory2+1
	bmi _branchlabel
	bne !fin+
	lda _memory1
	cmp _memory2
	bcs _branchlabel
!fin:
}

.macro CmpWordMem( _memory1, _memory2 )
{
	lda _memory1+1
	cmp _memory2+1
	bne !skip+
	lda _memory1
	cmp _memory2
!skip:
}

.macro AddWordConst( _memory, _value )
{
	lda _memory
	clc
	adc #_value
	sta _memory
.if ( _value < 256 )
{
	bcc !skip+
	inc _memory+1
}
else
{
	lda _memory+1
	adc #_value>>8
	sta _memory+1
}
!skip:
}

.macro AddWordMem( _memory1, _memory2, _destination )
{
	lda _memory1
	clc
	adc _memory2
	sta _destination
	lda _memory1+1
	adc _memory2+1
	sta _destination+1
}

.macro SubWordConst( _memory, _value )
{
	sec
	lda _memory
	sbc #[_value]
	sta _memory
	lda _memory+1
	sbc #[_value>>8]
	sta _memory+1
}

.macro SubWordMem( _memory1, _memory2 )
{
	lda _memory1
	sec
	sbc _memory2
	sta _memory1
	lda _memory1+1
	sbc _memory2+1
	sta _memory1+1
}

/* mamo byłem tutaj */
/* (c) skrzyp 2137  */


// This function returns the number of bits in a value that is 1
// _value can be up to 16 bits.

.function CountBitsSet( _value )
{
	.var bitcount = List().add( 0, 1, 1, 2, 1, 2, 2, 3, 1, 2, 2, 3, 2, 3, 3, 4 ).lock()
	// Raf : splitted these additions because they were thrown as ERROR in kickasm 4
	.var bits1 = bitcount.get( [ _value>>0 ] & $f ) 
	.var bits2 = bitcount.get( [ _value>>4 ] & $f )
	.var bits3 = bitcount.get( [ _value>>8 ] & $f )
	.var bits4 = bitcount.get( [ _value>>12] & $f )
	.return bits1 + bits2 + bits3 + bits4	
}

// Multiplies the A-register with a immediate value
// Alters registers A, Y
// Destination in A, overflow in _hibyte if != 0
.macro MultiplyConst( _factor, _optional_hibyte )
{
	.if ( _optional_hibyte != 0 )
	{
		ldy #0
		sty _optional_hibyte
	}
	.var firstBit = false
	.if ( CountBitsSet( _factor ) != 1 )
	{
		sta ZP_TEMP_MACRO0
	}
	.for ( var currbit = 0; currbit < 16; currbit++ )
	{
		.var mask = [1<<[15-currbit]]
		.if ( firstBit == true )
		{
			asl
			.if ( _optional_hibyte != 0 )
			{
				rol _optional_hibyte
			}
		}
		.if ( [_factor & mask] != 0 )
		{
			.if ( firstBit == false )
			{
				.eval firstBit = true
			}
			else
			{
				clc
				adc ZP_TEMP_MACRO0
				.if ( _optional_hibyte != 0 )
				{
					bcc !skip+
					inc _optional_hibyte
				!skip:
				}
			}
		}
	}
}

// This macro performs a multiplication of two memory locations
// \param _memory1 first memory to multiply [DESTROYED]
// \param _memory2 second memory to mulitply [DESTROYED]
// \param _destination where to store the result
// \param _temp_multiplier 2 bytes intermediate values
// \registers A
.macro MultiplyWordMem( _memory1, _memory2, _destination, _temp_multiplier )
{
.const MEMORY1_HIBYTE = _temp_multiplier
.const DESTINATION_LOBYTE = _temp_multiplier+1
	lda #0
	sta MEMORY1_HIBYTE
	sta DESTINATION_LOBYTE
	sta _destination
	sta _destination+1
untilempty:
	lsr _memory2+1
	ror _memory2
	bcc noAdd
	lda DESTINATION_LOBYTE
	clc
	adc _memory1
	sta DESTINATION_LOBYTE
	lda _destination
	adc _memory1+1
	sta _destination
	lda _destination+1
	adc MEMORY1_HIBYTE
	sta _destination+1
noAdd:
	asl _memory1
	rol _memory1+1
	rol MEMORY1_HIBYTE

	lda _memory2
	ora _memory2+1
	bne untilempty
}

.function factorial( _value )
{
  .if ( _value == 0 )
  {
    .return 0
  }
  .var rval = 1
  .for ( var i = 1; i <= _value; i++ )
  {
    .eval rval = rval * i
  }
  .for( var i = 0; i < 100; i++ )
  {
    .if ( rval >= 256 )
    {
      .eval rval = rval >> 1
    }
  }
  .return rval
}

