#importonce

// This file contains macros for utilizing the Ram Expansion Unit of the C64

.const REU_BASE    = $df00
.const REU_STATUS  = REU_BASE+0
.const REU_COMMAND = REU_BASE+1
.const REU_C64MEM  = REU_BASE+2
.const REU_REUMEM  = REU_BASE+4
.const REU_LENGTH  = REU_BASE+7
.const REU_IRQMASK = REU_BASE+9
.const REU_CONTROL = REU_BASE+10

// Control codes
.const REUC_FIX_NONE = %00000000
.const REUC_FIX_REU  = %01000000 // Fill C64 memory with value from REU
.const REUC_FIX_C64  = %10000000 // Stream from REU into C64 address
.const REUC_FIX_BOTH = %11000000

// Command codes
.const REUC_COPY_TO_REU     = %10010000
.const REUC_COPY_FROM_REU   = %10010001
.const REUC_COMPARE_WITH_REU= %10010011

.const REUC_AUTOLOAD      = %00100000 // 
.const REU_COMPARE_FAILED = %00100000

.macro REU_SetControl( _value )
{
	lda #_value
	sta REU_CONTROL
}

.macro REU_DoCommand( _command )
{
	lda #_command
	sta REU_COMMAND
}

.macro REU_KeepC64Pointer( _storage )
{
	lda REU_C64MEM+0
	sta _storage+0
	lda REU_C64MEM+1
	sta _storage+1
}

.macro REU_RestoreC64Pointer( _storage )
{
	lda _storage+0
	sta REU_C64MEM+0
	lda _storage+1
	sta REU_C64MEM+1
}

.macro REU_KeepREUPointer( _storage )
{
	lda REU_REUMEM+0
	sta _storage+0
	lda REU_REUMEM+1
	sta _storage+1
	lda REU_REUMEM+2
	sta _storage+2
}

.macro REU_Push( _storage )
{
	lda REU_C64MEM+0
	sta _storage+0
	lda REU_C64MEM+1
	sta _storage+1
	lda REU_REUMEM+0
	sta _storage+2
	lda REU_REUMEM+1
	sta _storage+3
	lda REU_REUMEM+2
	sta _storage+4
	lda REU_LENGTH+0
	sta _storage+5
	lda REU_LENGTH+1
	sta _storage+6
	lda REU_CONTROL
	sta _storage+7
}

.macro REU_Pop( _storage )
{
	lda _storage+0
	sta REU_C64MEM+0
	lda _storage+1
	sta REU_C64MEM+1
	lda _storage+2
	sta REU_REUMEM+0
	lda _storage+3
	sta REU_REUMEM+1
	lda _storage+4
	sta REU_REUMEM+2
	lda _storage+5
	sta REU_LENGTH+0
	lda _storage+6
	sta REU_LENGTH+1
	lda _storage+7
	sta REU_CONTROL
}

// Returnvalue in carry, set if REU is detected
.macro REU_detectREU()
{
	ldx #1
!loop:
	txa
	sta REU_C64MEM-1,x
	inx
	cpx #5
	bne !loop-
	ldx #1
!loop:
	txa
	cmp REU_C64MEM-1,x
	bne notexists
	inx
	cpx #5
	bne !loop-
	sec
	.byte LDA_IMM
notexists:
	clc
}

// This macro will examine the size of the reu and return a number according to how many double increments it can make
.const REU_SIZE0 = 0 // No REU installed
.const REU_SIZE64K = 1
.const REU_SIZE128K = 2
.const REU_SIZE256K = 3
.const REU_SIZE512K = 4
.const REU_SIZE1M = 5
.const REU_SIZE2M = 6
.const REU_SIZE4M = 7
.const REU_SIZE8M = 8
.const REU_SIZE16M = 9
// Returnvalue in register A
.macro REU_checkREUSize()
{
	lda #0
	sta blockCount+1
	sta memoryToWrite+1
	lda #1
	sta counter+1
	ldx #1
!loop:
	txa
	sta REU_C64MEM-1,x
	inx
	cpx #5
	bne !loop-
	ldx #1
!loop:
	txa
	cmp REU_C64MEM-1,x
	bne examinFailed
	inx
	cpx #5
	bne !loop-
	
	jmp memoryToWrite

examinFailed:
	jmp blockCount

memoryToWrite:
	lda #0
	sta REU_LENGTH+1
	sta REU_CONTROL
	sta REU_REUMEM+0
	sta REU_REUMEM+1
	sta REU_REUMEM+2
	lda #<memoryToWrite+1
	sta REU_C64MEM+0
	lda #>memoryToWrite+1
	sta REU_C64MEM+1
	lda #1
	sta REU_LENGTH+0
	lda #REUC_COPY_TO_REU | REUC_AUTOLOAD
	sta REU_COMMAND
	// Check if memory was written
	lda #REUC_COMPARE_WITH_REU | REUC_AUTOLOAD
	sta REU_COMMAND
	lda REU_STATUS
	and #REU_COMPARE_FAILED
	cmp #REU_COMPARE_FAILED
	bne incAndLoop
	jmp blockCount

doubleAndLoop:
	lda #1
	sta REU_LENGTH+0
counter:
	ldx #1
	lda #0
	sta REU_REUMEM+0
	sta REU_REUMEM+1
	stx REU_REUMEM+2
	lda #<memoryToWrite
	sta REU_C64MEM+0
	lda #>memoryToWrite
	sta REU_C64MEM+1
	lda #REUC_COPY_TO_REU | REUC_AUTOLOAD
	sta REU_COMMAND
	// Check if memory was written
	lda #REUC_COMPARE_WITH_REU | REUC_AUTOLOAD
	sta REU_COMMAND
	lda REU_STATUS
	and #REU_COMPARE_FAILED
	cmp #REU_COMPARE_FAILED
	beq blockCount
	// Check if memory wrapped
	lda #<memoryToWrite+1
	sta REU_C64MEM+0
	lda #>memoryToWrite+1
	sta REU_C64MEM+1
	lda #0
	sta REU_REUMEM+0
	sta REU_REUMEM+1
	sta REU_REUMEM+2
	lda #REUC_COMPARE_WITH_REU | REUC_AUTOLOAD
	sta REU_COMMAND
	lda REU_STATUS
	and #REU_COMPARE_FAILED
	cmp #REU_COMPARE_FAILED
	beq blockCount

	asl counter+1
incAndLoop:
	inc blockCount+1
	lda blockCount+1
	cmp #10
	beq blockCount
	jmp doubleAndLoop

blockCount:
	lda #0
}

.macro REU_StreamFromREU( _source, _destination, _amount )
{
	lda #<_destination
	sta REU_C64MEM+0
	lda #>_destination
	sta REU_C64MEM+1
	lda #_source>>0
	sta REU_REUMEM+0
	lda #_source>>8
	sta REU_REUMEM+1
	lda #_source>>16
	sta REU_REUMEM+2

	lda #<_amount
	sta REU_LENGTH+0
	lda #>_amount
	sta REU_LENGTH+1

	lda #REUC_STREAM_FROM_REU
	sta REU_CONTROL
	lda #REUC_COPY_FROM_REU
	sta REU_COMMAND

}

.macro REU_FillMemWithREU( _destination, _value, _amount )
{
	lda #<_destination
	sta REU_C64MEM+0
	lda #>_destination
	sta REU_C64MEM+1
	lda #_value
	sta REU_REUMEM+0
	lda #0
	sta REU_REUMEM+1
	sta REU_REUMEM+2
	
	lda #<_amount
	sta REU_LENGTH+0
	lda #>_amount
	sta REU_LENGTH+1


	lda #REUC_FILL_FROM_REU
	sta REU_CONTROL
	lda #REUC_COPY_FROM_REU
	sta REU_COMMAND
}

.macro REU_CopyToREU( _source, _destination, _amount )
{
	lda #<_source
	sta REU_C64MEM+0
	lda #>_source
	sta REU_C64MEM+1

	lda #_destination>>0
	sta REU_REUMEM+0
	lda #_destination>>8
	sta REU_REUMEM+1
	lda #_destination>>16
	sta REU_REUMEM+2

	lda #<_amount
	sta REU_LENGTH+0
	lda #>_amount
	sta REU_LENGTH+1

	lda #0
	sta REU_CONTROL

	lda #REUC_COPY_TO_REU
	sta REU_COMMAND
}

.macro REU_CopyFromREU( _source, _destination, _amount )
{
	lda #<_destination
	sta REU_C64MEM+0
	lda #>_destination
	sta REU_C64MEM+1

	lda #_source>>0
	sta REU_REUMEM+0
	lda #_source>>8
	sta REU_REUMEM+1
	lda #_source>>16
	sta REU_REUMEM+2

	lda #<_amount
	sta REU_LENGTH+0
	lda #>_amount
	sta REU_LENGTH+1

	lda #0
	sta REU_CONTROL

	lda #REUC_COPY_FROM_REU
	sta REU_COMMAND
}


.macro REU_MovePointerREU( _byvalue )
{
	lda REU_REUMEM+0
	clc
	adc #[_byvalue]>>0
	tax
	lda REU_REUMEM+1
	adc #[_byvalue]>>8
	tay
	lda REU_REUMEM+2
	adc #[_byvalue]>>16
	stx REU_REUMEM+0
	sty REU_REUMEM+1
	sta REU_REUMEM+2
}

.macro REU_MovePointerC64( _byvalue )
{
	lda REU_C64MEM+0
	clc
	adc #[_byvalue]>>0
	tax
	lda REU_C64MEM+1
	adc #[_byvalue]>>8
	stx REU_C64MEM+0
	sta REU_C64MEM+1
}


.macro REU_SetPointerREU( _value )
{
	lda #[_value]>>0
	sta REU_REUMEM+0
	lda #[_value]>>8
	sta REU_REUMEM+1
	lda #[_value]>>16
	sta REU_REUMEM+2
}

.macro REU_SetPointerC64( _value )
{
	lda #[_value]>>0
	sta REU_C64MEM+0
	lda #[_value]>>8
	sta REU_C64MEM+1;}


.macro REU_SetLength( _value )
{
	lda #[_value]>>0
	sta REU_LENGTH+0
	lda #[_value]>>8
	sta REU_LENGTH+1
}
