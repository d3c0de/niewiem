#importonce
.import source "zeropage.asm"
// This file contains semaphore macros
// Up to 8 individual signals can be chosen
// The signal zeropage used to communicate is $02

.const SIGNAL_ZP = $1a
:ReserveZP( SIGNAL_ZP, "SIGNAL_ZP" )

.macro InitSignal()
{
	lda #0
	sta SIGNAL_ZP
}

// Reset a signal to nonsignalled state
.macro ResetSignal( _mask )
{
	lda SIGNAL_ZP
	and #$ff-_mask
	sta SIGNAL_ZP	
}

// This macro will wait for any of the signals in _mask
// Any signals set in the mask will abort the wait, and
// the signals will be reset. After the macro returns, 
// A will contain the value of signals set in _mask
.macro WaitForSignal( _mask )
{
!loop:
	lda SIGNAL_ZP
	and #_mask
	beq !loop-
	tax
	eor SIGNAL_ZP
	sta SIGNAL_ZP
	txa
}

.macro HasSignal( _mask )
{
	lda SIGNAL_ZP
	and #_mask
	beq !finished+
	tax
	eor SIGNAL_ZP
	sta SIGNAL_ZP
	txa
!finished:
}

// Same as above, except it does a jump if any signal in _mask is set 
.macro PollSignal( _mask, _jumplocation )
{
	lda SIGNAL_ZP
	and #_mask
	beq !skip+
	jmp _jumplocation
!skip:		
}

.macro SetSignal( _mask )
{
	lda SIGNAL_ZP
	ora #_mask
	sta SIGNAL_ZP
}
