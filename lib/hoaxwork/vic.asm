#importonce
.import source "registers.asm"
// This macro will set the $d018 register to the correct  value for displaying
// a specific font and layout.
// \param _font location of the font to use (must be a multiple of $0800)
// \param _layout location of the layout to use (must be a multiple of $0400)
.macro SetFontAndLayout( _font, _layout )
{
.const _font_value = [ mod( _font, $4000 ) / $400 ] & $f
.const _layout_value = mod( _layout, $4000 ) / $400
	lda #_font_value + $10*_layout_value
	sta SCREEN_CHARACTER_MEMORY_ADDRESS
}

// This macro will set the VICBank to the selected bank.
// \param _bankNo number of the bank to use.
.macro SetVICBank( _bankNo )
{
	lda #$03
	ldx #_bankNo
	sta PORTA_SERIAL_BUS_DIRECTION
	stx PORTA_SERIAL_BUS_ACCESS
}

.function calculateBank( _data )
{
	.const bank = floor( _data/$4000 )
	.return 3-bank
}

.function calculateDisplayValue( _data, _layout )
{
	.const dataValue = [ mod( _data, $4000 ) / $400 ] & $f
	.const layoutValue = [ mod( _layout, $4000 ) / $400 ] & $f
	.return dataValue + $10*layoutValue
}

.macro SetVideo( _data, _layout )
{
	.const bankNo = 3-[ floor( _data/$4000 ) ]
	.const bankVerify = 3-[ floor( _layout/$4000) ]
	.if ( bankNo != bankVerify )
	{
		.error "data and layout in different banks"
	}
//	.print "Setting video to bank " + bankNo 
	lda #calculateDisplayValue( _data, _layout )
	ldx #3
	ldy #bankNo
	sta SCREEN_CHARACTER_MEMORY_ADDRESS
	stx PORTA_SERIAL_BUS_DIRECTION
	sty PORTA_SERIAL_BUS_ACCESS
}

.const DATAMODE = 1
.const COLORMODE = 2

.const BITMAP = 1
.const CHARACTER = 0
.const MULTICOLOR = 2
.const SINGLECOLOR = 0

.macro SetVideoMode( _modevalue )
{
	.if ( [ _modevalue & DATAMODE ] == BITMAP )
	{
		lda SCREEN_CONTROL_REGISTER_Y
		and #%01111111
		ora #%00110000
		sta SCREEN_CONTROL_REGISTER_Y
	} else
	{
		lda SCREEN_CONTROL_REGISTER_Y
		and #%01011111
		ora #%00010000
		sta SCREEN_CONTROL_REGISTER_Y
	}
	.if ( [ _modevalue & COLORMODE ] == MULTICOLOR )
	{
		lda SCREEN_CONTROL_REGISTER_X
		ora #%00010000
		sta SCREEN_CONTROL_REGISTER_X
	}
	else
	{
		lda SCREEN_CONTROL_REGISTER_X
		and #%11101111
		sta SCREEN_CONTROL_REGISTER_X
	}
}

.macro SetBackground( _border, _screen )
{
	lda #_border
	sta BORDER_COLOR_REGISTER
	lda #_screen
	sta BACKGROUND_COLOR_REGISTER
}

.function SpriteIndex( _memory )
{
	.return [ _memory / $40 ] & $ff
}

.macro CopyCharacterROM( _intoMemory )
{
	ldy PROCESSOR_PORT
	lda #PP_CHR_ONLY
	sta PROCESSOR_PORT
	ldx #0
!loop:
.for ( var loop = 0; loop < 8; loop++ )
{
	lda CHARACTER_ROM_BITMAP+$100*loop,x
	sta _intoMemory+$100*loop,x
}
	inx
	bne !loop-
	sty PROCESSOR_PORT
}
