#importonce
.var DEBUG = 0
.const SHOW_INTERRUPTTIME = 1<<0
.const SHOW_RASTERTIME    = 1<<1
.const TRACE_ZEROPAGE     = 1<<2
.const BREAK_ON_ERROR     = 1<<3

.macro WaitForDebug( _usememory )
{
	lda #1
	sta _usememory
!loop:
	inc $d020
	lda _usememory
	cmp #0
	bne !loop-
}

.macro SetDebug( _value )
{
	.eval DEBUG = DEBUG | _value
}

.function DebugOn( _value )
{
	.if ( [ DEBUG & _value ] != 0 )
	{
		.return true
	}
	.return false
}

.macro ShowRasterInc()
{
	.if ( [ DEBUG & SHOW_RASTERTIME ] != 0 )
	{
		inc $d020
	}
}

.macro ShowRasterDec()
{
	.if ( [ DEBUG & SHOW_RASTERTIME ] != 0 )
	{
		dec $d020
	}
}

.macro BreakWithError( _output_string_ )
{
	.if ( BREAK_ON_ERROR == true )
	{
	jmp !past_string+
!string:
	.text "error: "
	.text _output_string_
	.byte 0
!past_string:
	lda #$3f
	sta $00
	lda #$37
	sta $01
	jsr $ff81
	jsr $ff84
	jsr $ff8a
	// Output the string
	ldx #0
!repeat:
	lda !string-,x
	cmp #0
	beq !finished+
	sta $0400,x
	inx
	beq !finished+
	jmp !repeat-
!finished:
	jmp *
	}
}

.macro StartMeasureRasterTime( _tempstore )
{
	lda $d012
	sta _tempstore
	lda $d011
	:Lsr( 7 )
	sta _tempstore+1
}

.macro StopMeasureRasterTime( _tempstore, _permstore )
{
	lda $d012
	sta ZP_TEMP_MACRO0
	lda $d011
	:Lsr( 7 )
	sta ZP_TEMP_MACRO1
	:SubWordMem( ZP_TEMP_MACRO0, _tempstore )
	:CmpWordMem( ZP_TEMP_MACRO0, _permstore )
	bmi !skip+
	lda ZP_TEMP_MACRO0
	sta _permstore
	lda ZP_TEMP_MACRO1
	sta _permstore+1
!skip:
}

// Store the register A if it is higher than _memory
// Typically used to remember highest value from several iterations (i.e. rasterline)
.macro StoreIfHigher( _memory )
{
	cmp _memory
	bcc !skip+
	sta _memory
!skip:	
}

.macro PrintByte( _memorylocation, _spritedata )
{
}

.macro PrintWord( _memorylocation, _spritedata )
{
	lda PROCESSOR_PORT
	pha
	lda #PP_CHR_ONLY
	sta PROCESSOR_PORT
	lda _memorylocation+1
	and #$0f
	tax
	lda #0
	sta ZP_TEMP_MACRO1
	lda !table+,x
	asl
	rol ZP_TEMP_MACRO1
	asl
	rol ZP_TEMP_MACRO1
	asl
	rol ZP_TEMP_MACRO1
	sta ZP_TEMP_MACRO0
	lda ZP_TEMP_MACRO1
	clc
	adc #>CHARACTER_ROM_BITMAP
	sta ZP_TEMP_MACRO1
	
	ldx #0
	ldy #0
!loop:
	lda (ZP_TEMP_MACRO0),y
	sta _spritedata,x
	inx
	inx
	inx
	iny
	cpy #8
	bne !loop-

	lda _memorylocation
	lsr
	lsr
	lsr
	lsr
	tax
	lda #0
	sta ZP_TEMP_MACRO1
	lda !table+,x
	asl
	rol ZP_TEMP_MACRO1
	asl
	rol ZP_TEMP_MACRO1
	asl
	rol ZP_TEMP_MACRO1
	sta ZP_TEMP_MACRO0
	lda ZP_TEMP_MACRO1
	clc
	adc #>CHARACTER_ROM_BITMAP
	sta ZP_TEMP_MACRO1
	
	ldx #0
	ldy #0
!loop:
	lda (ZP_TEMP_MACRO0),y
	sta _spritedata+1,x
	inx
	inx
	inx
	iny
	cpy #8
	bne !loop-
	
	lda _memorylocation
	and #$0f
	tax
	lda #0
	sta ZP_TEMP_MACRO1
	lda !table+,x
	asl
	rol ZP_TEMP_MACRO1
	asl
	rol ZP_TEMP_MACRO1
	asl
	rol ZP_TEMP_MACRO1
	sta ZP_TEMP_MACRO0
	lda ZP_TEMP_MACRO1
	clc
	adc #>CHARACTER_ROM_BITMAP
	sta ZP_TEMP_MACRO1
	
	ldx #0
	ldy #0
!loop:
	lda (ZP_TEMP_MACRO0),y
	sta _spritedata+2,x
	inx
	inx
	inx
	iny
	cpy #8
	bne !loop-

	pla
	sta PROCESSOR_PORT
	jmp !end+
!table:
	.byte '0', '1', '2', '3', '4', '5', '6', '7'
	.byte '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'
!end:
}

