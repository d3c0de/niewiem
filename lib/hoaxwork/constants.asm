#importonce
// This file contains some basic constants that are useful to have a text representation of.

// Colors
.const COLOR_BLACK    = BLACK
.const COLOR_WHITE    = $1
.const COLOR_RED      = $2
.const COLOR_CYAN     = $3
.const COLOR_PURPLE   = $4
.const COLOR_GREEN    = $5
.const COLOR_BLUE     = $6
.const COLOR_YELLOW   = $7
.const COLOR_LT_BROWN = $8
.const COLOR_BROWN    = $9
.const COLOR_PINK     = $a
.const COLOR_DK_GRAY  = $b
.const COLOR_GRAY     = $c
.const COLOR_LT_GREEN = $d
.const COLOR_LT_BLUE  = $e
.const COLOR_LT_GRAY  = $f

// Multicolors
.const MULTICOLOR_BASE = $8
.const COLOR_BLACK_MULTICOLOR  = MULTICOLOR_BASE + COLOR_BLACK
.const COLOR_WHITE_MULTICOLOR  = MULTICOLOR_BASE + COLOR_WHITE
.const COLOR_RED_MULTICOLOR    = MULTICOLOR_BASE + COLOR_RED
.const COLOR_CYAN_MULTICOLOR   = MULTICOLOR_BASE + COLOR_CYAN
.const COLOR_PURPLE_MULTICOLOR = MULTICOLOR_BASE + COLOR_PURPLE
.const COLOR_GREEN_MULTICOLOR  = MULTICOLOR_BASE + COLOR_GREEN
.const COLOR_BLUE_MULTICOLOR   = MULTICOLOR_BASE + COLOR_BLUE
.const COLOR_YELLOW_MULTICOLOR = MULTICOLOR_BASE + COLOR_YELLOW

// Processor Port constants
.const PPA_MEMORY_ACCESS = %00000111
.const PPA_DEFAULT       = %00101111

// PP Enumeration (CR=Character ROM, BA=Basic, KE = KERNAL, IO = $D000 IO)
.const PP_RAM             = %00000000 | $30
.const PP_BAN_CRY_ION_KEN = %00000001 | $30
.const PP_BAN_CRY_ION_KEY = %00000010 | $30
.const PP_BAY_CRY_ION_KEY = %00000011 | $30
.const PP_RAM_DUPLICATE   = %00000100 | $30
.const PP_BAN_CRN_IOY_KEN = %00000101 | $30
.const PP_BAN_CRN_IOY_KEY = %00000110 | $30
.const PP_BAY_CRN_IOY_KEY = %00000111 | $30
// Common PP values
.const PP_DEFAULT = PP_BAY_CRN_IOY_KEY
.const PP_VIC_ONLY = PP_BAN_CRN_IOY_KEN
.const PP_CHR_ONLY = PP_BAN_CRY_ION_KEN
.const PP_RAM_ONLY = PP_RAM

// VIC Bank selection
.const VIC_BANK_0000 = 3
.const VIC_BANK_4000 = 2
.const VIC_BANK_8000 = 1
.const VIC_BANK_C000 = 0

.const CHARACTER_ROM = $1000
.const DEFAULT_LAYOUT = $0400
.const CHARACTER_ROM_BITMAP = $d000

// Rasters
.const RASTERLINE_FIRST_VISIBLE   = $10
.const RASTERLINE_FIRST_ONSCREEN  = $33
.const RASTERLINE_FIRST_OFFSCREEN = RASTERLINE_FIRST_ONSCREEN + 8*25
.const RASTERLINE_LAST_VISIBLE    = RASTERLINE_FIRST_OFFSCREEN + $23

// Sprites
.const SPRITE_SELECTION_OFFSET = $03f8 // Relative to current layout
.const SPRITE_POSY_TOP = $32
.const SPRITE_POSX_LEFTMOST = $18
.const SPRITE_POSY_BOTTOM = SPRITE_POSY_TOP + 200 - 21
.const SPRITE_POSX_RIGHTMOST = $18+320-24
.const SPRITE_HEIGHT = 21
.const SPRITE_WIDTH = 3
.const MEMORY_FOR_SPRITE = $40

// Layout
.const LAYOUT_HEIGHT = 25
.const LAYOUT_WIDTH = 40
.const LAYOUT_BYTES = LAYOUT_WIDTH*LAYOUT_HEIGHT

//######
// Bits

// Screen Control Register #1/Y ($d011)
.const SCRY_YPOS_MASK       = %00000111
.const SCRY_SCREEN_24ROWS   = 0
.const SCRY_SCREEN_25ROWS   = %00001000
.const SCRY_SCREEN_OFF      = 0
.const SCRY_SCREEN_ON       = %00010000
.const SCRY_TEXT_MODE       = 0
.const SCRY_BITMAP_MODE     = %00100000
.const SCRY_EXT_COLOR_OFF   = 0
.const SCRY_EXT_COLOR_ON    = %01000000
.const SCRY_RASTERHIGH_MASK = %10000000
.const SCRY_RASTERHIGH_OFF  = 0
.const SCRY_RASTERHIGH_ON   = %10000000
.const SCRY_YPOS_DEFAULT    = 3

// SCREEN_HORIZONTAL_AND_MULTICOLOR ($d016)
.const SCRX_XPOS_MASK        = %00000111
.const SCRX_SCREEN_38COLUMNS = 0
.const SCRX_SCREEN_40COLUMNS = %00001000
.const SCRX_SINGELCOLOR_MODE = 0
.const SCRX_MULTICOLOR_MODE  = %00010000
.const SCRX_XPOS_DEFAULT     = 0

// Interrupt status mask ($d019)
.const ISM_RASTER_INTERRUPT            = %00000001 // R/W
.const ISM_SPRITE_BACKGROUND_COLLISION = %00000010 // R/W
.const ISM_SPRITE_SPRITE_COLLISION     = %00000100 // R/W
.const ISM_LIGHTPEN                    = %00001000 // R/W
.const ISM_INTERRUPT_GENERATED         = %10000000 // R/O

// Interrupt request mask ($d01a)
.const IRM_RASTER_INTERRUPT            = %00000001
.const IRM_SPRITE_BACKGROUND_COLLISION = %00000010
.const IRM_SPRITE_SPRITE_COLLISION     = %00000100
.const IRM_LIGHTPEN                    = %00001000

// PortA Serial Bus Direction ($dd02)
.const SBD_BANK = %00000111

// D011 modes
.const DEFAULT_d011 = $1b
.const MODE_BITMAP = 1<<5
.const MODE_CHARACTER = 0

// Basic
.const BASIC_START                      = $0801
.const ASSEMBLY_START                   = $080d

