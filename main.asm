/* THIS FILE IS INTENTIONALLY CRAPPY
 *
 * As it's a main file, you should replace its contents by
 * BASIC startup and includes for all other files with correct
 * memory segments. KickAsss allows to use non-linear file structure,
 * so don't worry about ordering when your file is correctly placed
 * in memory and does not overlap anything!
 *
 */

#import "lib/hoaxwork/constants.asm"
#import "lib/hoaxwork/debug.asm"
#import "lib/hoaxwork/interrupt.asm"
#import "lib/hoaxwork/math.asm"
#import "lib/hoaxwork/memory.asm"
#import "lib/hoaxwork/registers.asm"
//#import "lib/hoaxwork/reu.asm"
#import "lib/hoaxwork/semaphore.asm"
#import "lib/hoaxwork/sort.asm"
#import "lib/hoaxwork/vic.asm"
//#import "lib/hoaxwork/zeropage.asm"

#import "const.asm"
#import "engine.asm"
#import "joystick.asm"	
#import "title.asm"

//:BasicUpstart2(loop.start)

	